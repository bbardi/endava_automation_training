package com.automationpractice;

import com.assertthat.selenium_shutterbug.core.Capture;
import com.assertthat.selenium_shutterbug.core.Shutterbug;
import com.automationpractice.enums.Title;
import com.automationpractice.pages.HomePage;
import com.sun.tools.javac.Main;
import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.ITestResult;
import org.testng.annotations.*;

import java.io.IOException;
import java.time.Duration;
import java.util.NoSuchElementException;
import java.util.Properties;
import java.util.Random;

public class MyTest {
    private static int counter = 0;

    private WebDriver driver;
    private HomePage homePage;
    @BeforeTest
    @Parameters("browser")
    public void setup (@Optional("chrome") String browser){
        String buildBrowser = System.getProperty("browser");
        if(buildBrowser != null){
            driver = DriverFactory.getDriver(buildBrowser);
        } else
            driver = DriverFactory.getDriver(browser);
    }

    @Test
    public void registerTest() {
        Logger LOG = LoggerFactory.getLogger(this.getClass());
        String projectUrl = System.getProperty("project.url");
        LOG.info("Navigate to {}", projectUrl);
        driver.get(projectUrl);
        homePage = new HomePage(driver);
        Random rand = new Random();

        homePage.goToSignIn()
                .register("testing-BB"+rand.nextInt()+"@example.com")
                .checkTitle(Title.MR)
                .fillPassword("MyPa$$w0rdIsStronk")
                .fillName("TestFName","TestLName")
                .fillBirthDate("29","1","1980")
                .fillAddress("TestFName","TestLName","Some address","Bear","12345","Delaware","12345678")
                        .clickSignUp();

        //if Sign in is displayed it means the registration
        Assertions.assertThrows(org.openqa.selenium.NoSuchElementException.class,()->driver.findElement(By.linkText("Sign in")));
        //Assert.assertEquals(driver.findElement(By.linkText("Sign in")).isDisplayed(),false);
    }

    @AfterMethod
    public void tearDown(ITestResult result){
        if(!result.isSuccess()){
            Shutterbug.shootPage(driver, Capture.FULL,500,true)
                    .withName("screenshot_"+(counter++)).save();
        }
    }

    @AfterTest
    public void cleanUp(){
        driver.quit();
    }
}

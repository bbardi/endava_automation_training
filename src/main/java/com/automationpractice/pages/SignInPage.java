package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class SignInPage extends BasePage{
    private final By email = By.id("email");
    private final By email_create = By.id("email_create");
    private final By password = By.id("passwd");
    private final By loginButton = By.id("SubmitLogin");
    private final By signupButton = By.id("SubmitCreate");

    public SignInPage(WebDriver driver) {
        super(driver);
        WebDriverWait wait = new WebDriverWait(driver, Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(email_create));
    }

    public UserPage logIn(String user, String passwd){
        driver.findElement(email).sendKeys(user);
        driver.findElement(password).sendKeys(passwd);
        driver.findElement(loginButton).click();
        return new UserPage(driver);
    }
    public RegisterPage register(String user){
        driver.findElement(email_create).sendKeys(user);
        driver.findElement(signupButton).click();
        return new RegisterPage(driver);
    }
}

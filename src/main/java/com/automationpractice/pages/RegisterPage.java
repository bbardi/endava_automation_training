package com.automationpractice.pages;

import com.automationpractice.enums.Title;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.Duration;

public class RegisterPage extends BasePage {
    private final By mrRadioButton = By.id("id_gender1");
    private final By mrsRadioButton = By.id("id_gender2");
    private final By custFirstName = By.id("customer_firstname");
    private final By custLastName = By.id("customer_lastname");
    private final By passwd = By.id("passwd");
    private final By birthDay = By.id("days");
    private final By birthMonth = By.id("months");
    private final By birthYear = By.id("years");
    private final By newsletterCheckbox = By.id("newsletter");
    private final By addressFirstName = By.id("firstname");
    private final By addressLastName = By.id("lastname");
    private final By addressAddress1 = By.id("address1");
    private final By addressCity = By.id("city");
    private final By addressZipCode = By.id("postcode");
    private final By addressState = By.id("id_state");
    private final By addressPhone = By.id("phone_mobile");
    private final By signUpButton = By.id("submitAccount");

    public RegisterPage(WebDriver driver) {
        super(driver);
        WebDriverWait wait = new WebDriverWait(driver,Duration.ofSeconds(20));
        wait.until(ExpectedConditions.presenceOfElementLocated(mrRadioButton));
    }

    public UserPage clickSignUp() {
        driver.findElement(signUpButton).click();
        return new UserPage(driver);
    }

    public RegisterPage checkTitle(Title title) {
        switch (title) {
            case MR:
                driver.findElement(mrRadioButton).click();
                break;
            case MRS:
                driver.findElement(mrsRadioButton).click();
                break;
        }
        return this;
    }

    public RegisterPage fillName(String firstName, String lastName) {
        driver.findElement(custFirstName).sendKeys(firstName);
        driver.findElement(custLastName).sendKeys(lastName);
        return this;
    }

    public RegisterPage fillBirthDate(String day, String month, String year) {
        Select daySel = new Select(driver.findElement(birthDay));
        Select monthSel = new Select(driver.findElement(birthMonth));
        Select yearSel = new Select(driver.findElement(birthYear));
        daySel.selectByValue(day);
        monthSel.selectByValue(month);
        yearSel.selectByValue(year);
        return this;
    }

    public RegisterPage fillPassword(String password) {
        driver.findElement(passwd).sendKeys(password);
        return this;
    }

    public RegisterPage checkNewsletter() {
        driver.findElement(newsletterCheckbox).click();
        return this;
    }

    public RegisterPage fillAddress(String firstName, String lastName, String address, String city,
                                    String zipcode, String state, String phone) {
        driver.findElement(addressFirstName).sendKeys(firstName);
        driver.findElement(addressLastName).sendKeys(lastName);
        driver.findElement(addressAddress1).sendKeys(address);
        driver.findElement(addressCity).sendKeys(city);
        driver.findElement(addressPhone).sendKeys(phone);
        driver.findElement(addressZipCode).sendKeys(zipcode);
        Select select = new Select(driver.findElement(addressState));
        select.selectByVisibleText(state);
        return this;
    }


}

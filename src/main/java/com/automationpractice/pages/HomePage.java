package com.automationpractice.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {
    private final By signInLink = By.cssSelector("a.login");

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public SignInPage goToSignIn(){
        driver.findElement(signInLink).click();
        return new SignInPage(driver);
    }
}
